'use strict'

const Path = require('path')
const Fs   = require('fs')
const Url  = require('url')

function StaticFile () {
	this.dir = ''
	this.statusCode = 200
	this.contentType = 'text/html'
}

// __dirname + '/public'
// /app/public
StaticFile.prototype.use = function (opt) {
	this.dir = opt.dir
	this.contentType = opt.contentType || 'text/html'

	if (typeof opt !== 'object') {
		this.dir = opt
		this.contentType = 'text/html'	
	}
}

StaticFile.prototype.serve = function (req, res) {
	let that = this
	let filename = req.url
	
	if (req.url === '/') {
		filename = '/index.html'
	}

	let file = that.dir + filename
	let body

	Fs.readFile(file, function (err, content) {
		if (err) {
			that.statusCode = 404
			res.statusMessage = 'Not Found'
			body = '404 Not Found'
		} else {
			body = content.toString()
		}

		res.writeHead(that.statusCode, { 'Content-Type' : that.contentType })
		res.end(body)

		console.log(`${req.method} ${req.url} ${res.statusCode} ${res.statusMessage}`)
	})
}

exports = module.exports = new StaticFile()