'use strict'

/*
 * Modules and dependencies
 */
const Http = require('http')
const Path = require('path')
const Url  = require('url')
const Fs   = require('fs')

/*
 * Local variables
 */
const StaticFiles = require('./lib/static-files')

let port = 8080
let filePath = Path.normalize(Path.join(__dirname, 'public'))
let file = Fs.readFileSync(`${filePath}/index.html`)

/*
 * Magic Happens
 */
StaticFiles.use(Path.join(__dirname, '/public'))

Http
	.createServer(onRequest)
	.listen(port, onListening)

/*
 * Helpers
 */
function onRequest (req, res) {
	StaticFiles.serve(req, res)
}

function onListening () {
	console.log('Server running at localhost:8080')
}