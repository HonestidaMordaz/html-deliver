var http = require('http');
var fs = require('fs');
var path = require('path');
var url = require('url')

var port = 8080

http.createServer(onRequest).listen(port, onListen);

/*
 * Helpers
 */
function onRequest (req, res) {
	var file = url.parse(req.url).path

	if (file === '/') {
		file = 'index.html'
	}
	
	var filePath = path.join(__dirname, 'public', file)
	filePathNormalized = path.normalize(filePath)

	var extname = path.extname(filePathNormalized);
	var contentType = 'text/html'
		
	switch (extname) {
		case '.js':
			contentType = 'text/javascript';
			break;
		case '.css':
			contentType = 'text/css';
			break;
	}
	
	fs.exists(filePathNormalized, function(exists) {	
		if (exists) {
			fs.readFile(filePath, function(error, content) {
				if (error) {
					res.writeHead(500);
					res.end();
				} else {
					res.writeHead(200, { 'Content-Type': contentType });
					res.end(content, 'utf-8');
				}
			});
		} else {
			res.writeHead(404);
			res.end();
		}
	});
}

function onListen () {
	console.log('Server running at http://127.0.0.1:8125/');
}